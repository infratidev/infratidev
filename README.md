# 📋 infratidev
## Learning about

* Kubernetes
* Containers
* Terraform
* Python
* Shell Script
* Powershell
* Cloud platforms, AWS, Azure, GCP
* CI/CD
* Ansible
* Ansible Molecule
* Puppet
* Vagrant
* Vault
* Packer
* Observability (ELK Stack,Graylog,Grafana,Prometheus)
* Google AppScripts
* ...

## Get in touch
- Website (Under Development): [infrati.dev](https://infrati.dev)
- Contact me: [infrati@infrati.dev](mailto:infrati@infrati.dev)
